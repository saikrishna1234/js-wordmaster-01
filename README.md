## Word Master

- Technologies used: HTML, CSS, JavaScript, API Calls.
- GET method is used to fetch a random five letter word which keeps changing everyday.
  https://words.dev-apis.com/word-of-the-day.
- POST method is used to check whether the user input word is a valid english word or not.
  https://words.dev-apis.com/validate-word
- The src includes the index.html, style.css, app.js(main), apicall, func.js.
- Used event listener "keydown"
- When ever the user click on the "Backspace" button. backspace() function will be triggered and the last letter of the word erases.
- When the user clicks "Enter" button. commit() function will be triggered. It validates the word if its a valid english word. It moves to next row otherwise displays message.
- Loader functionality is implemented while during apicall the spinner displays once the response is obtained from the api the spinner doesn't display.
- isLetter functionality is used to check whether the input is a letter or not.
- makeMap functionality is used to iterate over the array like ["A", "P", "P", "L", "E"] and it returns a object of {"A":1, "P":2, "L":1, "E":1} which helps in displaying background color of each letter accordingly.

## How does it works:

- There are six chances to guess the 5 letter word.
- If we guesed the word correctly within the 6 attempts we will win, otherwise we will lose the game.
- If the word is correct, It displays with color Green.
- If the word is wrong, It displays the word with grey color.
- If the letter in the word is present in the random word, It will displays with yellow color.

**Try Out Here**

https://subtle-lebkuchen-24a69a.netlify.app
