import { api, validate } from "./api.js";
import { makeMap } from "./func.js";

let wordParts = "";
const letters = document.querySelectorAll(".box");
const loadingDiv = document.querySelector(".loader");
let resultMsg = document.querySelector(".result-message");

const ANSWER_LENGTH = 5;
const ROUNDS = 6;

let init = async () => {
  let currentRow = 0;
  let currentGuess = "";
  let done = false;
  let isLoading = true;

  let word1 = await api();
  let wordParts = word1.split("");

  setLoading(false);

  function addLetter(letter) {
    if (currentGuess.length < ANSWER_LENGTH) {
      currentGuess += letter;
    }
    letters[currentRow * ANSWER_LENGTH + currentGuess.length - 1].innerText =
      letter;
  }

  let commit = async () => {
    setLoading(true);

    let validWord = await validate(currentGuess);
    setLoading(false);

    if (!validWord) {
      markInvalidWord();
      return;
    }

    const guessParts = currentGuess.split("");
    const map = makeMap(wordParts);
    let allRight = true;

    for (let i = 0; i < ANSWER_LENGTH; i++) {
      if (guessParts[i] === wordParts[i]) {
        letters[currentRow * ANSWER_LENGTH + i].classList.add("correct");
        map[guessParts[i]] -= 1;
      } else if (map[guessParts[i]] && map[guessParts[i]] > 0) {
        allRight = false;
        letters[currentRow * ANSWER_LENGTH + i].classList.add("close");
        map[guessParts[i]] -= 1;
      } else {
        allRight = false;
        letters[currentRow * ANSWER_LENGTH + i].classList.add("wrong");
      }
    }

    currentRow += 1;
    currentGuess = "";

    if (allRight) {
      resultMsg.textContent = `Congratulations`;
      done = true;
    } else if (currentRow === ROUNDS) {
      resultMsg.textContent = `You lose, the word was ${word1}`;
      done = true;
    }
  };

  function backspace() {
    currentGuess = currentGuess.substring(0, currentGuess.length - 1);
    letters[currentRow * ANSWER_LENGTH + currentGuess.length].innerText = "";
  }

  function markInvalidWord() {
    resultMsg.textContent = "Enter a valid word";
  }

  document.addEventListener("keydown", function handleKeyPress(event) {
    const action = event.key;

    if (action === "Enter") {
      commit();
    } else if (action === "Backspace") {
      backspace();
    } else if (isLetter(action)) {
      addLetter(action.toUpperCase());
    }
  });
};

function isLetter(letter) {
  return /^[a-zA-Z]$/.test(letter);
}

function setLoading(isLoading) {
  loadingDiv.classList.toggle("hidden");
}

makeMap(wordParts);

init();
